$(document).ready(function () {
    $('button').click(function() {
        var login = $('#login').val();
        var password = $('#password').val();
        if(!login || !password) return $('.error').text("Please fill login and password fields");
        $.ajax({
            method: "POST",
            url: "/login",
            data: { login, password }
        })
            .then(function(result) {
                console.log("result", result)
                window.location = '/ok';
            })
            .catch(function(res) {
                if(res.responseJSON.error) return $('.error').text(res.responseJSON.error);
                $('.error').text("Bad request");
            });

    })
});