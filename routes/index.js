var express = require('express');
var router = express.Router();
var redis = require("redis"),
    client = redis.createClient();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/ok', function(req, res, next) {
  res.render('ok');
});

router.post('/login', function(req, res, next) {
  console.log('req.body', req.body)
  if(req.body.login === "user" && req.body.password === "user") return res.send('ok');
  res.status(400).send({error: "Incorrect login or password"});
});

module.exports = router;
